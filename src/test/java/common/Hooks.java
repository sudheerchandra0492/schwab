package common;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import utilities.Driver;

import java.util.logging.Logger;

public class Hooks {
    private Driver driver;
    private final Logger LOGGER = Logger.getLogger(Hooks.class.getName());
    public Hooks(Driver driver) {
        this.driver = driver;
    }

    @Before
    public void beforeScenario() {
        driver.initialize();
    }

    @After
    public void afterScenario(Scenario scenario) throws Throwable{
        if (!Config.getBrowser().equalsIgnoreCase(Constants.NONE)) {
//            driver.embedScreenshot(scenario);
            new Driver().destroyDriver();
        }
    }
}
